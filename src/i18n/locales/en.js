// ENGLISH
export default {
  click_me: 'Click ME!',
  not_implemented: 'Not implemented',

  // SIDE BAR
  dashboard__sidebar_label: 'Dashboard',
  teams__sidebar_label: 'Teams',
  players__sidebar_label: 'Players',
  documents__sidebar_label: 'Documents',
  calendar__sidebar_label: 'Calendar',
  club__sidebar_label: 'Club',
  payments__sidebar_label: 'Payments',
  tournaments__sidebar_label: 'Tournaments',
  chat__sidebar_label: 'Chat',
  crm__sidebar_label: 'CRM',
  settings__sidebar_label: 'Settings',
};
