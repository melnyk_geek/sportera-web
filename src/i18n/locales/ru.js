// RUSSIAN
export default {
  click_me: 'Жми МЕНЯ!',
  not_implemented: 'Не реализовано',

  common_cta_save: 'Сохранить',

  // SIDE BAR
  dashboard__sidebar_label: 'Дэшборд',
  dashboard__page_title: 'Дэшборд',
  teams__sidebar_label: 'Команды',
  teams__page_title: 'Команды',
  teams__list_create_team_cta: 'Создать Команду',
  teams__list__modal_title: 'Создать Команду',
  teams__list__modal_team_name_label: 'Название',
  teams__list__modal_team_name_tip: 'Введите название команды',
  teams__list__modal_gender_label: 'Пол игроков',
  teams__list__modal_gender_woman_tip: 'Девочки',
  teams__list__modal_gender_man_tip: 'Мальчики',
  teams__list__modal_players_age_label: 'Возраст игроков',
  teams__list__modal_players_age_from_tip: 'Возраст от',
  teams__list__modal_players_age_till_tip: 'Возраст до',
  teams__list__modal_trainer_label: 'Тренер',
  teams__list__modal_trainer_tip: 'Выберите тренера',
  teams__list__modal_trainings_schedule_label: 'Расписание тренировок',
  players__sidebar_label: 'Игроки',
  players__page_title: 'Игроки',
  documents__sidebar_label: 'Документы',
  calendar__sidebar_label: 'Календарь',
  club__sidebar_label: 'Клуб',
  payments__sidebar_label: 'Платежи',
  tournaments__sidebar_label: 'Турниры',
  chat__sidebar_label: 'Чат',
  crm__sidebar_label: 'CRM',
  settings__sidebar_label: 'Настройки',

  players__add_player_cta: 'Добавить игрока',
};
