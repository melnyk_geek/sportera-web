// UKRAINIAN
export default {
  click_me: 'Тисни МЕНЕ!',
  not_implemented: 'Не реалізовано',

  // SIDE BAR
  dashboard__sidebar_label: 'Дэшборд',
  teams__sidebar_label: 'Команды',
  teams__list__modal_title: 'Создать Команду',
  players__sidebar_label: 'Игроки',
  documents__sidebar_label: 'Документы',
  calendar__sidebar_label: 'Календарь',
  club__sidebar_label: 'Клуб',
  payments__sidebar_label: 'Платежи',
  tournaments__sidebar_label: 'Турниры',
  chat__sidebar_label: 'Чат',
  crm__sidebar_label: 'CRM',
  settings__sidebar_label: 'Настройки',
};
