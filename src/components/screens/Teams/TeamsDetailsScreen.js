import React, {Component} from 'react';

import {Button, ContentToolbar, NavTabList, PageToolbar} from "../../common";
import {CreateTeamModal} from "../../modals";
import {connect} from "react-redux";
import * as TeamsActions from "../../../store/actions/teamsActions";


class TeamsDetailsScreen extends Component {
  state = {
    team: null
  };

  componentDidMount() {
    const {
      match: {
        params: {id}
      },
      getTeamById
    } = this.props;

    getTeamById(id);
  }

  render() {
    if (!this.props.team) {
      return null;
    }

    return (
      <div>
        <PageToolbar/>

        <div className="typo-card">
          <NavTabList/>

          <ContentToolbar/>

          <div className="settings-row">
            <div className="right-settings">
              <div>
                <div className="settings-item">
                  <Button/>
                </div>

                <CreateTeamModal
                />
              </div>
            </div>

          </div>


        </div>


      </div>
    );
  }
}

const TeamsDetailsScreenContainer = connect(({teams}) => ({
  team: teams.selectedTeam
}), {
  getTeamById: TeamsActions.getTeamById
})(TeamsDetailsScreen);

export {TeamsDetailsScreenContainer as TeamsDetailsScreen};