import React, {Component} from 'react';
import {connect} from "react-redux";

import * as TeamsActions from "../../../store/actions/teamsActions";

import i18n from '../../../i18n';
import {WeekDay} from "../../../constants";

import {Button, PageToolbar, TeamCard} from "../../common";
import {CreateTeamModal} from "../../modals";
import {Routes} from "../../../navigation/Routes";

const LOOK = {
  CARD: 'CARD',
  LIST: 'LIST'
};

class TeamsScreen extends Component {
  state = {
    look: LOOK.CARD,
    modalVisible: false
  };

  setLAFPressed = (look) => {
    this.setState({look});
  };

  actionButtonPressed = () => {
    this.setState({modalVisible: true});
  };

  renderLookToggle = () => (
    <div className="settings-item success-group-sm">
            <span
              className={`btn-sm btn-success ${this.state.look === LOOK.CARD && 'active'}`}
              onClick={() => this.setLAFPressed(LOOK.CARD)}
            >
                <i className="ft-block"/>
            </span>
      <span
        className={`btn-sm btn-success ${this.state.look === LOOK.LIST && 'active'}`}
        onClick={() => this.setLAFPressed(LOOK.LIST)}
      >
                <i className="ft-list-view"/>
            </span>
    </div>
  );

  renderTeams = () => {
    const {
      look
    } = this.state;

    const {
      teamsList,
      history
    } = this.props;

    const teamListUI = teamsList.map((team) => (
      <TeamCard
        key={`team_${team.id}`}
        photo={team.photo}
        name={team.name}
        size={team.clients_count}
        trainers={team.employee_programs.map(({employee_position: {employee: {first_name, last_name}}}) => `${first_name} ${last_name}`)}
        schedule={team.working_times.map(({day, from, to: till}) => ({
          day: WeekDay[day],
          from,
          till
        }))}
        onClick={() => history.push(Routes.TEAMS_DETAILS.replace(':id', team.id))}
      />
    ));

    switch (look) {
      case LOOK.CARD:
        return (
          <div className="team-card-wrap grid-view">
            {teamListUI}
          </div>
        );
      case LOOK.LIST:
        return (
          <div className="team-card-wrap list-view">
            {teamListUI}
          </div>
        );
      default:
        return null;
    }
  };

  componentDidMount() {
    this.props.retrieveTeamsList();
  }

  render() {
    const {
      modalVisible
    } = this.state;

    return (
      <div>
        <PageToolbar
          actions={[
            this.renderLookToggle(),
            (
              <div className="settings-item">
                <Button
                  icon="plus"
                  text={i18n.t('teams__list_create_team_cta')}
                  onClick={this.actionButtonPressed}
                />
              </div>
            )
          ]}
        />

        {this.renderTeams()}

        <CreateTeamModal
          visible={modalVisible}
          onClose={() => this.setState({modalVisible: false})}
        />
      </div>

    );
  }
}

const TeamsScreenContainer = connect(({teams}) => ({
  teamsList: teams.teamsList
}), {
  retrieveTeamsList: TeamsActions.retrieveTeamsList
})(TeamsScreen);

export {TeamsScreenContainer as TeamsScreen};