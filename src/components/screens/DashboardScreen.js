import React, {Component} from 'react';

import {PageToolbar} from "../common";

class DashboardScreen extends Component {
  render() {
    return (
      <div>
        <PageToolbar/>
        DASHBOARD SCREEN
      </div>
    );
  }
}

export {DashboardScreen};