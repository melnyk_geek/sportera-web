import React from 'react';

import './SearchButton.scss';

const SearchButton = () => (
  <div className="search-wrap">
    <i className="ft-search"></i>
  </div>
);

SearchButton.propTypes = {};

export {SearchButton};