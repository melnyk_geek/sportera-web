import React from 'react';

import './SearchInput.scss';

const SearchInput = () => (
  <form action="" className="page-search-form">
    <div className="form-control-wrap">
      <input className="form-control" placeholder="Введите запрос"/>
      <i
        className="ft-close"
      />
    </div>
  </form>
);

SearchInput.propTypes = {};

export {SearchInput};