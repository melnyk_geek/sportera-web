import React from 'react';
import PropTypes from 'prop-types';

import './Label.scss';

const Label = ({value, required}) => (
  <label className={required ? 'required' : ''}>
    {value}
  </label>
);

Label.propTypes = {
  value: PropTypes.string,
  required: PropTypes.bool
};

export {Label};