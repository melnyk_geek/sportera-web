import React from 'react';

import './Icons.scss';

const Icons = () => (
  <div
    className="icons-preview"
  >

    <i className="ft-add-photo">ft-add-photo</i>
    <i className="ft-arrow-down-filled">ft-arrow-down-filled</i>
    <i className="ft-arrow-down">ft-arrow-down</i>
    <i className="ft-arrow-left-filled">ft-arrow-left-filled</i>
    <i className="ft-arrow-right-filled">ft-arrow-right-filled</i>
    <i className="ft-arrow-up-filled">ft-arrow-up-filled</i>
    <i className="ft-block">ft-block</i>
    <i className="ft-calendar-alt">ft-calendar-alt</i>
    <i className="ft-calendar">ft-calendar</i>
    <i className="ft-camera">ft-camera</i>
    <i className="ft-cart">ft-cart</i>
    <i className="ft-chat">ft-chat</i>
    <i className="ft-check-round">ft-check-round</i>
    <i className="ft-close">ft-close</i>
    <i className="ft-club">ft-club</i>
    <i className="ft-command">ft-command</i>
    <i className="ft-comment">ft-comment</i>
    <i className="ft-dashboard">ft-dashboard</i>
    <i className="ft-crm">ft-crm</i>
    <i className="ft-desktop">ft-desktop</i>
    <i className="ft-docs">ft-docs</i>
    <i className="ft-down">ft-down</i>
    <i className="ft-edit">ft-edit</i>
    <i className="ft-exid-round">ft-exid-round</i>
    <i className="ft-filter">ft-filter</i>
    <i className="ft-info-round">ft-info-round</i>
    <i className="ft-letter-open">ft-letter-open</i>
    <i className="ft-load">ft-load</i>
    <i className="ft-mail-symbol">ft-mail-symbol</i>
    <i className="ft-mob">ft-mob</i>
    <i className="ft-eye">ft-eye</i>
    <i className="ft-new-doc">ft-new-doc</i>
    <i className="ft-new-file">ft-new-file</i>
    <i className="ft-payment">ft-payment</i>
    <i className="ft-phone">ft-phone</i>
    <i className="ft-player">ft-player</i>
    <i className="ft-plus-rounded">ft-plus-rounded</i>
    <i className="ft-plus">ft-plus</i>
    <i className="ft-question-round">ft-question-round</i>
    <i className="ft-remove-round">ft-remove-round</i>
    <i className="ft-save">ft-save</i>
    <i className="ft-search">ft-search</i>
    <i className="ft-settings">ft-settings</i>
    <i className="ft-toggler">ft-toggler</i>
    <i className="ft-skedule">ft-skedule</i>
    <i className="ft-tourney">ft-tourney</i>
    <i className="ft-user">ft-user</i>
  </div>
);

export {Icons};