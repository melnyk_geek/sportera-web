import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import './InputRadio.scss';

const InputRadio = ({id = uuid(), group, type, label, value, onChange}) => (
  <span>
		<input
      id={id}
      name={group}
      type={type}
      checked={value}
      onChange={() => onChange && onChange()}
    />
		<label htmlFor={id}>
            {label}
        </label>
	</span>
);

InputRadio.propTypes = {
  id: PropTypes.string,
  group: PropTypes.string,
  type: PropTypes.oneOf(['radio', 'checkbox']),
  label: PropTypes.string,
  value: PropTypes.bool
};

InputRadio.defaultProps = {
  type: 'radio'
};

export {InputRadio};