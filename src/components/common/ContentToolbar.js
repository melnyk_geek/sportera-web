import React from 'react';

import {ContentMainTitle} from "./ContentMainTitle";
import {SearchButton} from "./SearchButton";
import {Button} from "./Button";

import i18n from '../../i18n';
import './ContentToolbar.scss';

const ContentToolbar = () => (
  <div className="content-toolbar-wrap">

    <ContentMainTitle/>
    <SearchButton/>
    {/*<SearchInput/>*/}

    <div className="right-settings">
      <div className="settings-item">
        <Button
          icon="plus"
          text={i18n.t('players__add_player_cta')}
        />
      </div>
    </div>

  </div>


);

ContentToolbar.propTypes = {};

export {ContentToolbar};