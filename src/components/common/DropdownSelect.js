import React, {Component} from 'react';

import './DropdownSelect.scss';

class DropdownSelect extends Component {
  state = {
    visible: false
  };

  render() {
    const {
      options,
      value,
      onChange,
      placeholder
    } = this.props;

    const selected = options.find((option) => option.value === value);

    return (
      <div className="dropdown-select">
                <span
                  className="dropdown-toggle"
                  onClick={() => this.setState(({visible}) => ({visible: !visible}))}
                >
                    {selected ? selected.label : placeholder}
                    </span>
        <div className={`dropdown-menu ${!this.state.visible ? 'd-none' : ''}`}>
          {
            options.map((item, index) => (
              <span
                key={`option_${index}`}
                className="dropdown-item"
                onClick={() => {
                  this.setState({visible: false});
                  onChange && onChange(item.value);
                }}
              >
                                {item.label}
                            </span>
            ))
          }
        </div>
      </div>
    );
  }
}

DropdownSelect.defaultProps = {
  options: []
};

export {DropdownSelect};