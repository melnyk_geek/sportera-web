import React, {Component} from 'react';
import PropTypes from "prop-types";

import './TeamCard.scss';

class TeamCard extends Component {
  state = {
    trainersShow: false,
    scheduleShow: false,
  };

  render() {
    const {
      trainersShow,
      scheduleShow
    } = this.state;

    const {
      name,
      size,
      photo,
      trainers,
      schedule,
      onClick
    } = this.props;

    return (
      <div className="team-card">
        <div
          className="card-logo"
          onClick={() => onClick && onClick()}
        >
          <img
            src={photo}
            alt=""
            title=""
          />
          <div className="main-info">
            <h4>{name}</h4>
            <div className="players">
              <i className="ft-command"/>
              <span>{size}</span>
            </div>
          </div>
        </div>
        <div className={`card-data-wtap ${trainersShow || scheduleShow ? 'card-data-open' : ''}`}>
          <div className="card-data">
            <ul className="team-card-list">
              <li className={`card-list-dropdown ${trainersShow ? 'open' : ''}`}>
                <div className="team-card-item">
                  <div className="flex-column">
                    <span>Тренерa:</span>
                  </div>
                  <div className="flex-column">
                    {
                      trainers.map((trainer, index) => (
                        <p key={index}>{trainer}</p>
                      ))
                    }
                  </div>
                  <div
                    className="flex-column arrow-column"
                    onClick={() => this.setState(({trainersShow}) => ({trainersShow: !trainersShow}))}
                  >
                    <i className="ft-arrow-down-filled"/>
                  </div>
                </div>
              </li>
              <li className={`card-list-dropdown ${scheduleShow ? 'open' : ''}`}>
                <div className="team-card-item">
                  <div className="flex-column">
                    <span>Расписание тренировок</span>
                    <div className="schedule">
                      {
                        schedule.map((app, index) => (
                          <div
                            key={index}
                            className="schedule-row"
                          >
                            <span>{app.day}</span>
                            <p>{app.from} - {app.till}</p>
                          </div>
                        ))
                      }
                    </div>
                  </div>
                  <div
                    className="flex-column arrow-column"
                    onClick={() => this.setState(({scheduleShow}) => ({scheduleShow: !scheduleShow}))}
                  >
                    <i className="ft-arrow-down-filled"/>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

TeamCard.propTypes = {
  name: PropTypes.string,
  size: PropTypes.number,
  photo: PropTypes.string,
  trainers: PropTypes.arrayOf(PropTypes.string),
  schedule: PropTypes.arrayOf(PropTypes.shape({
    day: PropTypes.string,
    from: PropTypes.string,
    till: PropTypes.string,
  })),
  onClick: PropTypes.func
};

TeamCard.defaultProps = {
  trainers: [],
  schedule: []
};

export {TeamCard};