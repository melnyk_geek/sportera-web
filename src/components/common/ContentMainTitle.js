import React from 'react';

import './ContentMainTitle.scss';

const ContentMainTitle = () => (
  <h1>Игроки <span>3</span></h1>
);

ContentMainTitle.propTypes = {};

export {ContentMainTitle};