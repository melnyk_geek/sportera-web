import React from 'react';

import './NavTabList.scss';

const NavTabList = () => (
  <div>
    <ul className="nav-tab-list">
      <li>
                <span className="nav-tab-link current">
                    Игроки
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Информация
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Тренировки
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Тесты
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Платежи
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Матчи
                </span>
      </li>
      <li>
                <span className="nav-tab-link">
                    Документы
                </span>
      </li>
    </ul>
  </div>
);

NavTabList.propTypes = {};

export {NavTabList};