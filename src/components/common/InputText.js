import React from 'react';
import PropTypes from 'prop-types';

import './InputText.scss';

const InputText = ({placeholder, value, onChange}) => (
  <input
    className="form-control"
    placeholder={placeholder}
    value={value}
    onChange={({target: {value}}) => onChange && onChange(value)}
  />
);

InputText.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export {InputText};