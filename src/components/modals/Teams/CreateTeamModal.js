import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

import {trainersService} from '../../../domain';
import * as TeamsActions from "../../../store/actions/teamsActions";

import {
  AddPhoto,
  Button,
  DropdownSelect,
  InputRadio,
  InputText,
  Label,
  Modal,
  TextLink,
  TimeSchedule
} from '../../common';

import i18n from '../../../i18n';

import '../../common/close.scss';

class CreateTeamModal extends Component {
  state = {
    teamName: '',
    gender: false,
    ageFrom: null,
    ageTill: null,
    trainer: null,

    trainersList: []
  };

  renderActionButtons = () => (
    <Button
      text={i18n.t('common_cta_save')}
      onClick={() => {
        const {
          teamName,
          gender,
          ageFrom,
          ageTill,
          trainer
        } = this.state;

        const sFrom = new Date();
        const sTill = new Date();
        sTill.setMonth(sTill.getMonth() + 1);

        this.props.createTeam({
          name: teamName,
          genders: [0],
          age_from: ageFrom,
          age_to: ageTill,
          // trainer
          season_start: sFrom,
          season_end: sTill,
        });

        // this.props.onClose();
      }}
    />
  );

  inputChanged = (key, value) => {
    this.setState({
      [key]: value
    })
  };

  async componentDidMount() {
    const trainersList = await trainersService.getTrainersList();

    this.setState({trainersList});
  }

  render() {
    const {
      visible,
      onClose
    } = this.props;

    return (
      <Modal
        visible={visible}
        title={i18n.t('teams__list__modal_title')}
        actions={this.renderActionButtons()}
        onClose={onClose}
      >
        <div className="bordered-box">
          <div className="row">
            <div className="col-md-6">
              <AddPhoto/>
            </div>
            {/*col-md-6*/}
            <div className="col-md-6">
              <div className="form-group mb20">
                <div className="d-block">
                  <Label value={i18n.t('teams__list__modal_team_name_label')} required={true}/>
                </div>
                <div className="d-block">
                  <InputText
                    placeholder={i18n.t('teams__list__modal_team_name_tip')}
                    value={this.state.teamName}
                    onChange={(value) => this.inputChanged('teamName', value)}
                  />
                </div>
              </div>
              {/*form-group*/}

              <div className="form-group mb15">
                <div className="d-block">
                  <Label value={i18n.t('teams__list__modal_gender_label')}/>
                </div>
                <div className="d-block">
                  <InputRadio
                    group="gender"
                    label={i18n.t('teams__list__modal_gender_woman_tip')}
                    value={!this.state.gender}
                    onChange={() => this.inputChanged('gender', false)}
                  />
                  <InputRadio
                    group="gender"
                    label={i18n.t('teams__list__modal_gender_man_tip')}
                    value={this.state.gender}
                    onChange={() => this.inputChanged('gender', true)}
                  />
                </div>
              </div>
              {/*form-group*/}

              <div className="form-group mb20">
                <div className="d-block">
                  <Label value={i18n.t('teams__list__modal_players_age_label')}/>:
                </div>
                <div className="d-block">
                  <div className="row-sm">
                    <div className="col-sm">
                      <DropdownSelect
                        options={[...new Array(100)].map((key, idx) => ({
                          value: idx + 1,
                          label: idx + 1
                        }))}
                        placeholder={i18n.t('teams__list__modal_players_age_from_tip')}
                        value={this.state.ageFrom}
                        onChange={(value) => this.inputChanged('ageFrom', value)}
                      />
                    </div>
                    {/*col-sm*/}
                    <div className="col-sm">
                      <DropdownSelect
                        options={[...new Array(100)].map((key, idx) => ({
                          value: idx + 1,
                          label: idx + 1
                        }))}
                        placeholder={i18n.t('teams__list__modal_players_age_till_tip')}
                        value={this.state.ageTill}
                        onChange={(value) => this.inputChanged('ageTill', value)}
                      />
                    </div>
                    {/*col-sm*/}
                  </div>
                  {/*row-sm*/}
                </div>
                {/*d-block*/}
              </div>
              {/*form-group*/}

              <div className="form-group mb0">
                <div className="d-block">
                  <Label value={i18n.t('teams__list__modal_trainer_label')}/>
                </div>
                {/*d-block*/}
                <div className="d-block">
                  <div className="row-sm align-items-stretch">
                    <div className="col-sm">
                      <DropdownSelect
                        options={this.state.trainersList.map((trainer) => ({
                          value: trainer.id,
                          label: `${trainer.first_name} ${trainer.last_name}`
                        }))}
                        placeholder={i18n.t('teams__list__modal_trainer_tip')}
                        value={this.state.trainer}
                        onChange={(value) => this.inputChanged('trainer', value)}
                      />
                      {/*dropdown-select*/}
                    </div>
                    {/*col-sm*/}

                    <div className="close-wrap">
                      <div className="close">
                        <i className="ft-close"/>
                      </div>
                    </div>
                    {/*close-wrap*/}

                  </div>
                  {/*row-sm*/}
                  <TextLink/>
                </div>
                {/*d-block*/}
              </div>
              {/*form-group*/}
            </div>
            {/*col-md-6*/}
          </div>
          {/*row*/}
        </div>
        {/*bordered-box*/}

        <div className="bordered-box">
          <div className="row">
            <div className="col">
              <Label value={i18n.t('teams__list__modal_trainings_schedule_label')}/>
            </div>
            {/*col*/}
          </div>
          {/*row*/}

          <div className="row">

            <div className="col-md-6">
              <div className="row">

                <div className="col">
                  <div className="dropdown-select">
                    <span className="dropdown-toggle">День недели</span>
                    <div className="dropdown-menu d-none">
                      <span className="dropdown-item">Select option1</span>
                      <span className="dropdown-item">Select option2</span>
                      <span className="dropdown-item">Select option3</span>
                      <span className="dropdown-item">Select option4</span>
                    </div>
                  </div>
                </div>
                {/*col*/}

                <div className="col">
                  <TimeSchedule/>
                </div>
                {/*col*/}
              </div>
              {/*row-sm*/}
            </div>
            {/*col-md-6*/}

            <div className="col-md-6">
              <div className="row-sm">
                <div className="col-sm">
                  <div className="dropdown-select">
                    <span className="dropdown-toggle">День недели</span>
                    <div className="dropdown-menu d-none">
                      <span className="dropdown-item">Select option1</span>
                      <span className="dropdown-item">Select option2</span>
                      <span className="dropdown-item">Select option3</span>
                      <span className="dropdown-item">Select option4</span>
                    </div>
                  </div>
                  {/*dropdown-select*/}
                </div>
                {/*col-sm*/}
                <div className="close-wrap">
                  <div className="close">
                    <i className="ft-close"/>
                  </div>
                </div>
                {/*close-wrap*/}
              </div>
              {/*row-sm*/}
            </div>
            {/*col-md-6*/}

          </div>
          {/*row*/}
          <TextLink/>
        </div>
        {/*bordered-box*/}
      </Modal>
    );
  }
}

CreateTeamModal.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
};

const CreateTeamModalContainer = connect(({trainers}) => ({
  //
}), {
  createTeam: TeamsActions.createTeam
})(CreateTeamModal);

export {CreateTeamModalContainer as CreateTeamModal};