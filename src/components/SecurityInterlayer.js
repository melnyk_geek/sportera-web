import React from 'react';
import {connect} from "react-redux";
import {Route, withRouter} from "react-router-dom";

import * as AuthActions from "../store/actions/authActions";

import './App.scss';

const SecurityInterlayer = ({component: Component, authorized, loaded, ...rest}) => {
  if (!loaded || !authorized) {
    return null;
  }

  return (
    <Route
      {...rest}
      render={(props) => (
        <Component
          {...props}
        />
      )}
    />
  );
};

const SecurityInterlayerContainer = withRouter(connect(({app, auth}) => ({
  loaded: app.loaded,
  authorized: !!auth.accessToken
}), {
  signIn: AuthActions.signInWithEmailAndPassword
})(SecurityInterlayer));

export {SecurityInterlayerContainer as SecurityInterlayer};
