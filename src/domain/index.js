import axios from 'axios';

import {API_URL} from "../utils/config";

import {AuthGateway, TeamsGateway, TrainersGateway} from "./gateways";
import {AuthService, TeamsService, TrainersService} from "./services";

const restAdapter = axios.create({
  baseURL: API_URL
});

const fileAdapter = axios.create({
  baseURL: API_URL
});

const adapters = {
  restAdapter,
  fileAdapter
};

const authGateway = new AuthGateway(adapters);
const teamsGateway = new TeamsGateway(adapters);
const trainersGateway = new TrainersGateway(adapters);

const gateways = {
  authGateway,
  teamsGateway,
  trainersGateway
};

const authService = new AuthService(gateways);
const teamsService = new TeamsService(gateways);
const trainersService = new TrainersService(gateways);

export {
  authService,
  teamsService,
  trainersService
};

export function applyAccessToken(accessToken) {
  restAdapter.defaults.headers['X-Session-Id'] = fileAdapter.defaults.headers['X-Session-Id'] = accessToken;
  fileAdapter.defaults.headers.post['Content-Type'] = 'multipart/form-data';
}