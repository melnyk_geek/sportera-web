import * as APIPaths from "../APIPaths";

class TrainersGateway {
  constructor({restAdapter}) {
    this.restAdapter = restAdapter;
  }

  /**
   * Get teams list.
   */
  async getTrainersList() {
    const {data} = await this.restAdapter.get(APIPaths.TRAINERS);

    return data;
  }
}

export {TrainersGateway};