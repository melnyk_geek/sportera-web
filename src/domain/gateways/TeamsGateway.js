import * as APIPaths from "../APIPaths";

class TeamsGateway {
  constructor({restAdapter}) {
    this.restAdapter = restAdapter;
  }

  /**
   * Get teams list.
   */
  async getTeamsList() {
    const {data} = await this.restAdapter.get(APIPaths.TEAMS);

    return data;
  }

  async getTeamById(id) {
    const {data} = await this.restAdapter.get(`${APIPaths.TEAMS}/${id}`);

    return data;
  }

  async createTeam(payload) {
    const {data} = await this.restAdapter.post(`${APIPaths.TEAMS}`, payload);

    return data;
  }
}

export {TeamsGateway};