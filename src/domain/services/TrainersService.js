class TrainersService {
  constructor({trainersGateway}) {
    this.trainersGateway = trainersGateway;
  }

  getTrainersList() {
    return this.trainersGateway.getTrainersList();
  }
}

export {TrainersService};