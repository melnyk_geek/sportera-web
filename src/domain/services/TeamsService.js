class TeamsService {
  constructor({teamsGateway}) {
    this.teamsGateway = teamsGateway;
  }

  getTeamsList() {
    return this.teamsGateway.getTeamsList();
  }

  getTeamById(id) {
    return this.teamsGateway.getTeamById(id);
  }

  createTeam(payload) {
    return this.teamsGateway.createTeam(payload);
  }
}

export {TeamsService};