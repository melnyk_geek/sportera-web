import * as Types from "../types";

export const retrieveTrainersList = () => ({type: Types.TRAINERS_GET_LIST_REQUEST});
export const retrieveTrainersListSuccess = (data) => ({type: Types.TRAINERS_GET_LIST_SUCCESS, data});
export const retrieveTrainersListFailure = (error) => ({type: Types.TRAINERS_GET_LIST_FAILURE, error});