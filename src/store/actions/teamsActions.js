import * as Types from "../types";

export const retrieveTeamsList = () => ({type: Types.TEAMS_GET_LIST_REQUEST});
export const retrieveTeamsListSuccess = (data) => ({type: Types.TEAMS_GET_LIST_SUCCESS, data});
export const retrieveTeamsListFailure = (error) => ({type: Types.TEAMS_GET_LIST_FAILURE, error});

export const getTeamById = (id) => ({type: Types.TEAMS_GET_TEAM_BY_ID_REQUEST, id});
export const getTeamByIdSuccess = (data) => ({type: Types.TEAMS_GET_TEAM_BY_ID_SUCCESS, data});
export const getTeamByIdFailure = (error) => ({type: Types.TEAMS_GET_TEAM_BY_ID_FAILURE, error});

export const createTeam = (data) => ({type: Types.TEAMS_CREATE_TEAM_REQUEST, data});
export const createTeamSuccess = (data) => ({type: Types.TEAMS_CREATE_TEAM_SUCCESS, data});
export const createTeamFailure = (error) => ({type: Types.TEAMS_CREATE_TEAM_FAILURE, error});