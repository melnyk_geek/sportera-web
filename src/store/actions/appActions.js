import * as Types from "../types";

export const appLoaded = () => ({
  type: Types.APP_LOADED
});