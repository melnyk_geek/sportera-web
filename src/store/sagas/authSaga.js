import {call, fork, put, takeLatest} from 'redux-saga/effects';
import {REHYDRATE} from 'redux-persist';
import {push} from 'connected-react-router';

import {Persistor} from '../index';

import * as Types from "../types";
import * as AppActions from "../actions/appActions";
import * as AuthActions from "../actions/authActions";

import {applyAccessToken, authService} from "../../domain";

function* processRestoreState({payload}) {
  try {
    const {
      auth: {accessToken}
    } = payload;

    if (accessToken) {
      yield processSetAccessToken(accessToken);
      // yield put(push('/'));
    } else {
      yield put(push('/login'));
    }
  } catch (e) {
    console.warn(e);
  }

  yield put(AppActions.appLoaded());
}

function* processSetAccessToken(accessToken) {
  yield call(() => applyAccessToken(accessToken));
}

function* processSignInWithEmailAndPassword({email, password}) {
  try {
    const data = yield call(() => authService.signIn({email, password}));

    yield put(AuthActions.applyAccessToken(data.token));

    yield put(AuthActions.signInWithEmailAndPasswordSuccess(data));

    yield put(push('/'));
  } catch (error) {
    yield put(AuthActions.signInWithEmailAndPasswordFailure(error));
  }
}

function* processSignOut() {
  Persistor.purge();
  yield put(push('/login'));
}

// -------------------------------
function* authListener() {
  yield takeLatest(REHYDRATE, processRestoreState);

  yield takeLatest(Types.AUTH_SIGN_IN_REQUEST, processSignInWithEmailAndPassword);
  yield takeLatest(Types.AUTH_APPLY_ACCESS_TOKEN, processSetAccessToken);

  yield takeLatest(Types.AUTH_SIGN_OUT, processSignOut);
}

export default function* authSaga() {
  yield fork(authListener);
}
