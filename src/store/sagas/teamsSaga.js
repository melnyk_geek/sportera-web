import {call, fork, put, takeLatest} from 'redux-saga/effects';

import {teamsService, trainersService} from "../../domain";
import * as Types from "../types";
import * as TeamsActions from "../actions/teamsActions";
import * as TrainersActions from "../actions/trainersActions";

function* processRetrieveTeamsList() {
  try {
    const resp = yield call(() => teamsService.getTeamsList());

    yield put(TeamsActions.retrieveTeamsListSuccess(resp));
  } catch (error) {
    yield put(TeamsActions.retrieveTeamsListFailure(error));
  }
}

function* processRetrieveTrainersList() {
  try {
    const resp = yield call(() => trainersService.getTrainersList());

    yield put(TrainersActions.retrieveTrainersListSuccess(resp));
  } catch (error) {
    yield put(TrainersActions.retrieveTrainersListFailure(error));
  }
}

function* processGetTeamById({id}) {
  try {
    const resp = yield call(() => teamsService.getTeamById(id));

    yield put(TeamsActions.getTeamByIdSuccess(resp));
  } catch (error) {
    yield put(TeamsActions.getTeamByIdFailure(error));
  }
}

function* processCreateTeam({data}) {
  try {
    const formData = new FormData();

    Object.keys(data).forEach((key) => formData.append(`program[${key}]`, data[key]));

    const resp = yield call(() => teamsService.createTeam(formData));

    yield put(TeamsActions.getTeamByIdSuccess(resp));
  } catch (error) {
    yield put(TeamsActions.getTeamByIdFailure(error.response));
  }
}

function* teamsListener() {
  yield takeLatest(Types.TEAMS_GET_LIST_REQUEST, processRetrieveTeamsList);
  yield takeLatest(Types.TEAMS_GET_TEAM_BY_ID_REQUEST, processGetTeamById);
  yield takeLatest(Types.TEAMS_CREATE_TEAM_REQUEST, processCreateTeam);
  yield takeLatest(Types.TRAINERS_GET_LIST_REQUEST, processRetrieveTrainersList);
}

export default function* teamsSaga() {
  yield fork(teamsListener);
}