import {fork, takeLatest} from 'redux-saga/effects';
import {REHYDRATE} from "redux-persist";

function* processAppLoaded() {
  // TODO: Handle loading state of app.
}

function* appListener() {
  yield takeLatest(REHYDRATE, processAppLoaded);
}

export default function* appSaga() {
  yield fork(appListener);
};
