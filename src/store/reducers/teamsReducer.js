import * as Types from "../types/index";

const initialState = {
  teamsList: [],
  selectedTeam: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.TEAMS_GET_LIST_SUCCESS:
      return {
        ...state,
        teamsList: action.data
      };
    case Types.TEAMS_GET_TEAM_BY_ID_SUCCESS:
      return {
        ...state,
        selectedTeam: action.data
      };
    default:
      return state;
  }
};