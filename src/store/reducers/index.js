import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';

import app from './appReducer';
import auth from './authReducer';
import teams from './teamsReducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  app,
  auth,
  teams
});