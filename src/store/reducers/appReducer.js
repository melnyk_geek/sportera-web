import * as Types from "../types/index";

const initialState = {
  loaded: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.APP_LOADED:
      return {
        ...state,
        loaded: true
      };
    default:
      return state;
  }
};