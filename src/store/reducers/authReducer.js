import * as Types from "../types/index";

const initialState = {
  accessToken: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.AUTH_APPLY_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.accessToken
      };
    case Types.AUTH_SIGN_OUT:
      return {
        ...initialState
      };
    default:
      return state;
  }
};