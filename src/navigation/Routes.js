const Routes = {
  DASHBOARD: '/dashboard',
  TEAMS: '/teams',
  TEAMS_DETAILS: '/teams/details/:id',
  PLAYERS: '/players',
  DOCUMENTS: '/documents',
  CALENDAR: '/calendar',
  CLUB: '/club',
  PAYMENTS: '/payments',
  TOURNAMENTS: '/tournaments',
  CHAT: '/chat',
  CRM: '/crm',
  SETTINGS: '/settings',
};

export {Routes};