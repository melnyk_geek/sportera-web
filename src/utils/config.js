global.__DEV__ = process.env.NODE_ENV === 'development';

export const API_URL = global.__DEV__
  ? 'http://beanstalk-staging.seqpubr7vy.eu-central-1.elasticbeanstalk.com/api'
  : 'http://beanstalk-staging.seqpubr7vy.eu-central-1.elasticbeanstalk.com/api';